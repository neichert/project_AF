This project folder contains tractography masks for human and macaque in standard space and the results presented in the paper. Tractography results are stored in the subfolder /volume and surface results in the subfolder /surface. Results of the vertex-wise lateralization analysis using palm are stored in the /human/surface subfolder. The TPO patch for the macaque surface and the transformed patch for the human surface are stored in /macaque/surface.
The numerical results for lateralization and DV index are stored in a matlab file 'stats_lat_and_DV.mat'.

If you have any questions, please contact nicole.eichert@psy.ox.ac.uk
