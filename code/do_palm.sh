# -----
# perform permutation analysis using PALM to perform the surface projections
# Before running this script, run process_surfaces for smoothing and log-normalize
# -----

# where to find the study
DD=/my_path_data/
# out directory for palm results
OD=/my_path_output/
# group directory for surface averages
GD=/my_path_surface/

# where to find PALM
PALMCMD=~/code/external/PALM/palm

n=25

m=af_avg

# copy files to palm folder

# Use the individual smoothed data as inputs
for s in $(seq 1 $n); do
  cp $DD/subj$s/${m}_l_S4.func.gii $OD/${s}_${m}_l_S4.func.gii
  cp $DD/subj$s/${m}_r_S4.func.gii $OD/${s}_${m}_r_S4.func.gii
done

# concatenate the subject surfaces into 4D files
for tract in ${m} ; do
  for hemi in l r ; do
    cmdStr=""
    for s in $(seq 1 $n); do
      cmdStr="$cmdStr -metric $OD/${s}_${tract}_${hemi}_S4.func.gii"
    done
    wb_command -metric-merge $OD/group_${tract}_${hemi}_S4.func.gii $cmdStr
  done
  # flip the right to the left hemisphere to allow them to be concatenated
  wb_command -set-structure $OD/group_${tract}_r_S4.func.gii CORTEX_LEFT
  # concatenate the hemispheres
  wb_command -metric-merge $OD/group_${tract}_S4.func.gii -metric $OD/group_${tract}_l_S4.func.gii -metric $OD/group_${tract}_r_S4.func.gii
  # note: the concatenated files consist then of 50 volumes
done

# specify a surface
surf=subj1.L.white.32k_fs_LR.surf.gii

# ignore the surface area (all vertices get a weigth of 1)
surfArea=1

# get a design matrix
# run Glm_gui.... (here design file used)
design=design25_lvsr/design

# calling PALM on AF
$PALMCMD -o $OD/palmES_${m}_S4 -i $OD/group_${m}_S4.func.gii -s $surf 1 -d $design.mat -t $design.con -n 10000 -saveglm -logp

## process palm results
# !! note: rename palm results to func.nii




# mask results

# mask based on p-values > 0.05
# logarithmized p-values, thus use threshold of -log10(0.05)=1.301
wb_command -metric-math "(x>1.301)" $OD/palmES_${m}_S4_dpv_tstat_fwep_c1_tP.func.gii -var x $OD/palmES_${m}_S4_dpv_tstat_fwep_c1.func.gii
wb_command -metric-math "(x>1.301)" $OD/palmES_${m}_S4_dpv_tstat_fwep_c2_tP.func.gii -var x $OD/palmES_${m}_S4_dpv_tstat_fwep_c2.func.gii

maskP1=$OD/palmES_${m}_S4_dpv_tstat_fwep_c1_tP.func.gii
maskP2=$OD/palmES_${m}_S4_dpv_tstat_fwep_c2_tP.func.gii

# mask based on AF projections
for t in 0.93; do
  wb_command -metric-math "(x>$t)" af_avg_l_S4_logNorm_t${t}.func.gii -var x af_avg_l_S4_logNorm.func.gii
  mask=${m}_l_S4_logNorm_t${t}.func.gii
  wb_command -metric-mask $OD/palm_${m}_S4_dpv_tstat_fwep_c1.func.gii $mask $OD/palm_${m}_S4_dpv_tstat_fwep_c1_masked.t${t}.func.gii
  wb_command -metric-mask $OD/palm_${m}_S4_dpv_tstat_fwep_c2.func.gii $mask $OD/palm_${m}_S4_dpv_tstat_fwep_c2_masked.t${t}.func.gii
  done

  wb_command -metric-mask $OD/palmES_${m}_S4_dpv_cohen_c1.func.gii $mask $OD/palmES_${m}_S4_dpv_cohen_c1_masked.t${t}.func.gii
  wb_command -metric-mask $OD/palmES_${m}_S4_dpv_cohen_c2.func.gii $mask $OD/palmES_${m}_S4_dpv_cohen_c2_masked.t${t}.func.gii

  wb_command -metric-mask $OD/palmES_${m}_S4_dpv_cohen_c1_masked.t${t}.func.gii $maskP1 $OD/palmES_${m}_S4_dpv_cohen_c1_masked.t${t}P.func.gii
  wb_command -metric-mask $OD/palmES_${m}_S4_dpv_cohen_c2_masked.t${t}.func.gii $maskP2 $OD/palmES_${m}_S4_dpv_cohen_c2_masked.t${t}P.func.gii
