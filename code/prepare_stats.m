% compute lateralization and DV indices for both tracts, both species, both modalities

% define parameters
OD='/my_path';
specs = {'human' 'macaque'};
tracts = {'af_avg','ifo'};
modalities = {'V','S'};

% loop over species
for isp = 1:2
spec=specs{isp};
fprintf(['do species:' spec '\n'])

% species specific settings

% where is the data located
DD=['my_path/' spec];

switch spec
    case 'human'
        subs = {'subj1', 'subj2','subj3','subj4','subj5','subj6','subj7','subj8','subj9','subj10',...
        'subj11','subj12','subj13','subj14','subj15','subj16','subj17','subj18','subj19','subj20',...
        'subj21','subj22','subj23','subj24', 'subj25'};
        n=25;
    case 'macaque'
        subs = {'hilary', 'oddie', 'rolo', 'umberto', 'decresp'};
        n=5;
end

% loop over modalities: V=volume space, S=surface space
for imod =1:2
    mod=modalities{imod};
    fprintf(['do modality: ' mod '\n'])

    % initialize cell arrays
    left=[];
    right=[];

    % loop over tracts
    for im = 1:2
        m=tracts{im};
        fprintf(['do tract: ' m '\n'])

        % initialize cell arrays
        lat=[];
        left{im}=[];
        right{im}=[];

        % loop over subjects
        for s = 1:length(subs)
            % chose input file depending on modality
            switch mod
                case 'V'
                    % volume
                    data_l=readimgfile([DD '/' subs{s} '/tracts/' m '_l/density.nii.gz']);
                    data_r=readimgfile([DD '/' subs{s} '/tracts/' m '_r/density.nii.gz']);
                case 'S'
                    data_l=getfield(gifti([DD '/' subs{s} '/' m '_l.func.gii']),'cdata');
                    data_r=getfield(gifti([DD '/' subs{s} '/' m '_r.func.gii']),'cdata');
            end

            % append subject data for this subject to cell array storing all subjects
            % data for each tract will be stored as matrix-item within the cell array
            left{im}=[left{im} data_l(:)];
            right{im}=[right{im} data_r(:)];

            % compute lateralization for the subject and store in the variable lat
            lat(im,s)=asymmetry(data_r,data_l);

        end % for subjects

        % compute statistic: signrank (Wilcoxon signed rank test)
        % for difference of the two hemispheres
        [signrankP,signrankH,signrankSTATS] = signrank(sum(right{im},1)',sum(left{im},1)','method','approximate');
        % for the lateralization being different from zero
        [signrankzeroP,signrankzeroH,signrankzeroSTATS] = signrank(lat(im,:));

        % write out group-level results to structure
        results.(spec).(m).(mod).lat_mean = mean(lat(im,:));
        results.(spec).(m).(mod).lat_se = std(lat(im,:))/sqrt(n);
        results.(spec).(m).(mod).signrankP = signrankP;
        results.(spec).(m).(mod).signrankES = signrankSTATS.zval/sqrt(n);

    end % for tract

    % compute group-level DV index (d-v) / (d+v)
    % left hemisphere
    dv_left=(sum(left{1},1)- sum(left{2},1)) ./ (sum(left{1},1) + sum(left{2},1));
    % right hemisphere
    dv_right=(sum(right{1},1)- sum(right{2},1)) ./ (sum(right{1},1) + sum(right{2},1));
    % total brain
    dv_tot=( (sum(left{1},1)+ sum(right{1},1)) - (sum(left{2},1)+ sum(right{2},1)) )  ./ ( (sum(left{1},1)+ sum(right{1},1)) + (sum(left{2},1)+ sum(right{2},1)) );

    % compute stats for DV index
    [dv_left_signrankzeroP,dv_left_signrankzeroH,dv_left_signrankzeroSTATS] = signrank(dv_left');
    [dv_right_signrankzeroP,dv_right_signrankzeroH,dv_right_signrankzeroSTATS] = signrank(dv_right');
    [dv_tot_signrankzeroP,dv_v_tot_signrankzeroH,dv_tot_signrankzeroSTATS] = signrank(dv_tot');

    [dv_leftvstot_signrankP,dv_leftvstot_signrankH,dv_leftvstot_signrankSTATS] = signrank(dv_left',dv_tot');
    [dv_rightvstot_signrankP,dv_rightvstot_signrankH,dv_rightvstot_signrankSTATS] = signrank(dv_right',dv_tot');
    [dv_leftvsright_signrankP,dv_leftvsright_signrankH,dv_leftvsright_signrankSTATS] = signrank(dv_left',dv_right','method','approximate');

    % write out results
    results.(spec).DV.(mod).left_mean=mean(dv_left);
    results.(spec).DV.(mod).right_mean=mean(dv_right);
    results.(spec).DV.(mod).tot_mean=mean(dv_tot);
    results.(spec).DV.(mod).left_se=std(dv_left)/sqrt(n);
    results.(spec).DV.(mod).right_se=std(dv_right)/sqrt(n);
    results.(spec).DV.(mod).tot_se=std(dv_tot)/sqrt(n);
    results.(spec).DV.(mod).leftvsright_signrankP=dv_leftvsright_signrankP;
    results.(spec).DV.(mod).leftvsright_signrankSTATS=dv_leftvsright_signrankSTATS;
    results.(spec).DV.(mod).leftvsright_signrankES=dv_leftvsright_signrankSTATS.zval/sqrt(n);

    end % for modality
end % for species

save('stats_results_avg','results');
