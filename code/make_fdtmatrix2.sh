 # Generate surfseed from macaque surface by calling quicktrack_gpu

 # wrapper for quicktrack_gpu
 # Create a midthicknes_surface*brain fdt_matrix2
 # the output can be used for multiplication
 # create a (cortex) x (whole brain) matrix by seeding probabilistic streamlines in standard space from every cortical vertex
 # and recording the number of samples reaching each brain voxel
 # This is done using the “matrix2” mode in probtrackx2


for hemi in L; do

  subjects="hilary rolo decresp umberto decresp"

  # loop over subjects
  for subj in $subjects;do
  bpxdir=$d/Macaque/data/${subj}.bedpostX
  outdir=$subj/surfseed_$hemi

  mkdir -p $outdir

  # surface
  mt=$f99/surf_macaque/${hemi}.midthickness.surf.gii

  std2diff=$bpxdir/xfms/transf_F99_to_anat_warp
  diff2std=$bpxdir/xfms/transf_anat_to_F99_warp

  o=" --xfm=$std2diff --invxfm=$diff2std"
  o=" $o --seed=$mt --omatrix2 --target2=$f99/mri/struct1mm_brain -P 10000 --seedref=$f99/mri/struct1mm "
  quicktrack_gpu $bpxdir $outdir $o
  done
done
