#! /bin/sh

# Run autoPtx in human subjects for the tracts defined in structureList

# data directory
DD=~/my_path
# out directory
OD=~/my_path_out

for subj in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25; do
    bpxdir=$DD/subj$subj/T1w/Diffusion.bedpostX
    std2diff=$DD/subj$subj/MNINonLinear/xfms/standard2acpc_dc
    diff2std=$DD/subj$subj/MNINonLinear/xfms/acpc_dc2standard

    fsl_autoPtx \
    -bpx $bpxdir \
    -out $OD \
  	-str $DD/structureList \
    -p $DD/protocols \
  	-stdwarp $std2diff $diff2std -res 2

done
