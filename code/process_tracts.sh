#!/usr/bin/env bash
umask u+rw,g+rw # give group read/write permissions to all new files

spec='human'
m=af

# where to find the study
OD=/my_path/

for hemi in l r; do

  if [ $spec == 'human' ]
  then
    DD=human
    subs=('subj1' 'subj2' 'subj3' 'subj4' 'subj5' 'subj6' 'subj7' 'subj8' 'subj9' 'subj10' \
    'subj11' 'subj12' 'subj13' 'subj14' 'subj15' 'subj16' 'subj17' 'subj18' 'subj19' 'subj20' \
    'subj21' 'subj22' 'subj23' 'subj24' 'subj25')
    n=25
  elif [ $spec == 'macaque' ]
  then
    DD=macaque
    subs=('hilary'  'oddie'  'rolo'  'umberto' 'decresp')
    n=5
  fi

  # Run this section only once:
  # average normal and inverted (ninvert) af tract
  for hemi in l r; do
    for s in $(seq 1 $n); do
      subj=${subs[$s-1]}
      # make subfolder to store averaged results
      mkdir $DD/${subj}/tracts/${m}_avg_${hemi}

      # average normal and inverted density into subfolder
      fsladd $DD/${subj}/tracts/af_avg_${hemi}/density.nii.gz -m $DD/${subj}/tracts/af_${hemi}/density.nii.gz $DD/${subj}/tracts/af_ninvert_${hemi}/density.nii.gz
    done # s
  done # hemi

  # log normalize the tract density and store as density_cert
  for s in $(seq 1 $n); do
    subj=${subs[$s-1]}
    fslmaths $DD/$subj/tracts/${m}_${hemi}/density.nii.gz -log $DD/$subj/tracts/${m}_${hemi}/density_cert.nii.gz
    fslmaths $DD/$subj/tracts/${m}_${hemi}/density_cert.nii.gz  -div `fslstats $DD/$subj/tracts/${m}_${hemi}/density_cert.nii.gz -R | awk '{print $2}'` $DD/$subj/tracts/${m}_${hemi}/density_cert.nii.gz
  done # s

  # average individual subject data in to ouptut group folder
  fsladd $OD/${m}_${hemi}_density -m $DD/*/tracts/${m}_${hemi}/density.nii.gz
  fsladd $OD/${m}_${hemi}_density_cert -m $DD/*/tracts/${m}_${hemi}/density_cert.nii.gz
  fsladd $OD/${m}_${hemi}_densityNorm -m $DD/*/tracts/${m}_${hemi}/densityNorm.nii.gz

  # resample macaque data from 1mm to 0.5mm space
  if [ $spec == 'macaque' ]
  then
    F99=F99_restore_brain.nii.gz
    mat=1mm_2_0.5mm.mat
    flirt -in $OD/${m}_${hemi}_density -out $OD/${m}_${hemi}_density_0.5mm -ref $F99 -applyxfm -init $mat -interp trilinear
    flirt -in $OD/${m}_${hemi}_density_cert -out $OD/${m}_${hemi}_density_cert_0.5mm -ref $F99 -applyxfm -init $mat -interp trilinear
    flirt -in $OD/${m}_${hemi}_densityNorm -out $OD/${m}_${hemi}_densityNorm_0.5mm -ref $F99 -applyxfm -init $mat -interp trilinear
  fi

done # hemi
