#!/usr/bin/env bash
umask u+rw,g+rw # give group read/write permissions to all new files

spec='human'
m=af

# where to find the study
DD=/my_path/
# group directory for averages
GD=$DD/$spec/group

if [ $spec == 'human' ]
then
	subs=('subj1' 'subj2' 'subj3' 'subj4' 'subj5' 'subj6' 'subj7' 'subj8' 'subj9' 'subj10' \
	'subj11' 'subj12' 'subj13' 'subj14' 'subj15' 'subj16' 'subj17' 'subj18' 'subj19' 'subj20' \
	'subj21' 'subj22' 'subj23' 'subj24' 'subj25')
	n=25
	kernel=4
elif [ $spec == 'macaque' ]
then
	subs=('hilary'  'oddie'  'rolo'  'umberto' 'decresp')
  n=5
	kernel=2
  surf_L=L.white.surf.gii
	surf_R=R.white.surf.gii
	thr=0.9
fi

#### smooth data

for s in $(seq 1 $n); do
	subj=${subs[$s-1]}

	# use individual surface for humans (macaque: high quality surface)
  if [ $spec == 'human' ]
  then
    surf_L=$HCP_Q2/$subj/MNINonLinear/fsaverage_LR32k/${subj}.L.white.32k_fs_LR.surf.gii
	  surf_R=$HCP_Q2/$subj/MNINonLinear/fsaverage_LR32k/${subj}.R.white.32k_fs_LR.surf.gii
  fi
	# define variables
	in_L=$DD/$spec/$subj/${m}_l.func.gii
	in_R=$DD/$spec/$subj/${m}_r.func.gii
	in_L_S=$DD/$spec/$subj/${m}_l_S$kernel.func.gii
	in_R_S=$DD/$spec/$subj/${m}_r_S$kernel.func.gii

	# smooth the data
	fsl_sub -q veryshort.q wb_command -metric-smoothing $surf_L $in_L $kernel $in_L_S
	fsl_sub -q veryshort.q wb_command -metric-smoothing $surf_R $in_R $kernel $in_R_S
done

# wait for some time so that the scipt continues when all jobs are finished
sleep 20

# create an average map of the smoothed surface data
avg_L=$GD/${m}_l_S$kernel.func.gii
avg_R=$GD/${m}_r_S$kernel.func.gii

wb_command -metric-math "(x)" $avg_L -var x $DD/$spec/${subs[0]}/${m}_l_S$kernel.func.gii
wb_command -metric-math "(x)" $avg_R -var x $DD/$spec/${subs[0]}/${m}_r_S$kernel.func.gii

for s in $(seq 2 $n); do
	subj=${subs[$s-1]}
  wb_command -metric-math "(x+y)" $avg_L -var x $avg_L -var y $DD/$spec/$subj/${m}_l_S$kernel.func.gii
  wb_command -metric-math "(x+y)" $avg_R -var x $avg_R -var y $DD/$spec/$subj/${m}_r_S$kernel.func.gii
done

wb_command -metric-math "x/$n" $avg_L -var x $avg_L
wb_command -metric-math "x/$n" $avg_R -var x $avg_R

# log normalize data
avg_L_ln=$GD/${m}_l_S${kernel}_logNorm.func.gii
avg_R_ln=$GD/${m}_r_S${kernel}_logNorm.func.gii

wb_command -metric-math "(log(x+1))" $avg_L_ln -var "x" $avg_L
wb_command -metric-math "(log(x+1))" $avg_R_ln -var "x" $avg_R
max_L=`wb_command -metric-stats $avg_L_ln -reduce MAX`
max_R=`wb_command -metric-stats $avg_R_ln -reduce MAX`
wb_command -metric-math "(x/$max_L)" $avg_L_ln -var x $avg_L_ln
wb_command -metric-math "(x/$max_R)" $avg_R_ln -var x $avg_R_ln

# threshold data
avg_L_t=$GD/${m}_l_S${kernel}_logNorm_t$thr.func.gii
avg_R_t=$GD/${m}_r_S${kernel}_logNorm_t$thr.func.gii

# generate temporary mask file
tmpdir=$(mktemp -d "/tmp/afproj.XXXXXXXXXX")
wb_command -metric-math "(x>$thr)" $tmpdir/mask_l.func.gii -var x $avg_L_ln
wb_command -metric-math "(x>$thr)" $tmpdir/mask_r.func.gii -var x $avg_R_ln
wb_command -metric-mask $avg_L_ln $tmpdir/mask_l.func.gii $avg_L_t
wb_command -metric-mask $avg_R_ln $tmpdir/mask_r.func.gii $avg_R_t

rm -rf $tmpdir

# assign hemisphere as structure
wb_command -set-structure $avg_L CORTEX_LEFT
wb_command -set-structure $avg_L_ln CORTEX_LEFT
wb_command -set-structure $avg_L_t CORTEX_LEFT

wb_command -set-structure $avg_R CORTEX_RIGHT
wb_command -set-structure $avg_R_ln CORTEX_RIGHT
wb_command -set-structure $avg_R_t CORTEX_RIGHT
