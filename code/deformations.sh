#!/usr/bin/env bash

# this script contains commands to transform between macaque, F99, pals and 32k space

# --------
# prepare
# -------
# create spheres (they are not fs_LR space)
wb_command -surface-create-sphere 164000 164k.L.sphere.surf.gii
wb_command -set-structure 164k.L.sphere.surf.gii CORTEX_LEFT
wb_command -surface-create-sphere 32000 32k.L.sphere.surf.gii
wb_command -set-structure 32k.L.sphere.surf.gii CORTEX_LEFT

# 73 surface from sumsdb: MACAQUE.F99UA1.ATLAS.03-06/LEFT_HEM/Macaque.F99UA1.Cerebral.L.ATLAS.03-06-03.34132.spec

# resample 32k macaque surface to 73k
sphere_32k=32k.L.sphere.surf.gii
sphere_164k=164k.L.sphere.surf.gii
sphere_73k=73k.L.sphere.surf.gii
my_surf=L.very_inflated.surf.gii
my_surf_73k=L.very_inflated_73k.surf.gii
wb_command -surface-resample $my_surf $sphere_32k $sphere_73k BARYCENTRIC $my_surf_73k
wb_command -surface-flip-normals $my_surf_73k $my_surf_73k

# convert .surf to .topo and .coord.gii
caret_command -file-convert -sc -is GS F99.left.very_inflated.surf.gii -os CARET F99.L.very_inflated.coord.gii F99.L.very_inflated.topo.gii VERY_INFLATED CLOSED -struct left
caret_command -file-convert -sc -is GS L.very_inflated_73k.surf.gii -os CARET L.very_inflated_73k.coord.gii L.very_inflated_73k.topo.gii PIAL CLOSED -struct left

# create deformation field
caret_command -deformation-map-create SPHERE \
L.very_inflated_73k.coord.gii \
L.very_inflated_73k.topo.gii \
F99.L.very_inflated.coord.gii \
F99.L.very_inflated.topo.gii \
macaque2F99.very_inflated.deform_map

# and inverse
caret_command -deformation-map-create SPHERE \
F99.L.very_inflated.coord.gii \
F99.L.very_inflated.topo.gii \
L.very_inflated_73k.coord.gii \
L.very_inflated_73k.topo.gii \
F992macaque.very_inflated.deform_map

# transform TPO patch from F99 to macaque
caret_command -deformation-map-apply F992macaque.very_inflated.deform_map SURFACE_SHAPE TPOp_NE_old.func.gii TPOp_NE.func.gii
wb_command -metric-resample TPOp_NE.func.gii $sphere_73k $sphere_32k BARYCENTRIC TPOp_NE.func.gii


# Continue here if the above has been run once
# --------
# deformation
# -------

macaque_2_F99=macaque2F99.very_inflated.deform_map
base=af_avg_l_S2_logNorm

# resample metric from 32k to 73k space
input_32k=${base}.func.gii
input_73k=${base}_73k.func.gii
sphere_32k=32k.L.sphere.surf.gii
sphere_73k=73k.L.sphere.surf.gii
wb_command -metric-resample $input_32k $sphere_32k $sphere_73k BARYCENTRIC $input_73k

# apply deformation macaque to F99 space
outputF99=${base}_F99.func.gii
caret_command -deformation-map-apply $macaque_2_F99 SURFACE_SHAPE $input_73k $outputF99

# deform F99 to pals
F99_2_Pals=RegF99toPALS_23-LDMK-VE_Macaque.F99UA1.RIGHT.Register-with-PALS_23-LDMK-VE.73730.spec.deform_map
dataFileType=METRIC_NEAREST_NODE
outputPals=${base}_pals.func.gii
caret_command -deformation-map-apply $F99_2_Pals $dataFileType $outputF99 $outputPals

# deform pals to 164k
output_164k=${base}_164k.func.gii
#cd ~/code/external/inter-atlas_deformation_maps/fs_LR
Pals_2_164k=pals.R.registered-to-fs_LR_fix.164k_fs_LR.deform_map
caret_command -deformation-map-apply $Pals_2_164k $dataFileType $outputPals $output_164k

# resample to 32k_fs_LR
CurrentSphere=fsaverage.R_LR.spherical_std.164k_fs_LR.surf.gii
NewSphere=R.sphere.32k_fs_LR.surf.gii
Method=ADAP_BARY_AREA # or BARYCENTRIC
AreaMetricsCurrent=fs_LR.R.midthickness_va_avg.164k_fs_LR.shape.gii
AreaMetricsNew=fs_LR.R.midthickness_va_avg.32k_fs_LR.shape.gii
output_32k=${base}_32k.func.gii
wb_command -metric-resample $output_164k $CurrentSphere $NewSphere $Method $output_32k -area-metrics $AreaMetricsCurrent $AreaMetricsNew
wb_command -set-structure $output_32k CORTEX_LEFT

# --------
# postprocessing
# -------

# create borders from metrics for figure
# TPOp_NE
surf=Q1-Q6_R440.L.white.32k_fs_LR.surf.gii
wb_command -metric-rois-to-border $surf TPOp_NE_32k.func.gii $base TPOp_NE_32k.border

# AF
wb_command -metric-math "(x>0.9)" af_avg_l_S2_logNorm_32k_t.9.func.gii -var x af_avg_l_S2_logNorm_32k.func.gii
wb_command -metric-rois-to-border $surf af_avg_l_S2_logNorm_32k_t.9.func.gii af_thr af_thr.border
