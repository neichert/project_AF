#! /bin/sh

# Run autoPtx in macaque subjects for the tracts defined in structureList


# data directory
DD=~/my_path
# out directory
OD=~/my_path_out

for subj in hilary oddie rolo umberto decresp; do
     bpxdir=$DD/${subj}/dMRI.bedpostX
     std2diff=$DD/$bpxdir/xfms/transf_F99_to_anat_warp
     diff2std=$DD/$bpxdir/xfms/transf_anat_to_F99_warp

     echo "note: running probtrackx2 with reduced steplength"
     fsl_autoPtx_macaque \
     -bpx $bpxdir \
     -out $OD \
 	   -str $DD/structureList \
	   -p $DD/protocols \
	   -stdwarp $std2diff $diff2std \
     -res 1

 done
