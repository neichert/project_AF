% project fdt_paths to surface

spec='human';
tract='af';
hemis={'l','r'};
OD='my_path'

switch spec
	case 'human'
	DD=['human'];
	subs = {'subj1', 'subj2','subj3','subj4','subj5','subj6','subj7','subj8','subj9','subj10',...
	'subj11','subj12','subj13','subj14','subj15','subj16','subj17','subj18','subj19','subj20',...
	'subj21','subj22','subj23','subj24', 'subj25'};
	matrix2{1} = '/AVG_Matrix2_L_1245subj.mat';
	matrix2{2} = '/AVG_Matrix2_R_1245subj.mat';
	standard = 'MNI152_T1_2mm_brain.nii.gz';
	;;
case 'macaque'
	subs = {'hilary', 'oddie', 'rolo', 'umberto', 'decresp'};
	DD='macaque';
	matrix2{1} = '/vols/Data/rbmars/temptract/descrep_surf/AVG_Matrix2_L_5macaques.mat';
	matrix2{2} = '/vols/Data/rbmars/temptract/descrep_surf/AVG_Matrix2_R_5macaques.mat';
	standard = 'F991mm_brain.nii.gz';
	;;
end

for s = 1:length(subs)
	for h = 1:2
	if hemis{h}=='l'; H='L';else H='R';end
        fprintf(['do ' subs{s} ' ' H '\n']);
	    multiply_fdt_NE('fdt_matrix2', matrix2{h},...
		'fdt_paths', [DD '/' subs{s} '/tracts/' tract '_' hemis{h} '/density.nii.gz'],...
		'mask', standard,...
		'outputname', [OD '/' subs{s} '/' tract '_' hemis{h} '.func.gii'],...
		'hemi', H);
	end
end
