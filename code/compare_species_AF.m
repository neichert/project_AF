clc
clear all

% script to compare the AF projections in humans and macaques.
% This section has been dropped in the final manuscript

tract='af_avg';
hemi='l';

n_vertices=32492;
n_subs_h=25;
n_subs_m=5;

subs_m={'hilary' 'umberto' 'rolo' 'oddie' 'decresp'};

outdir=fullfile(filesep,'Users','neichert','scratch','MScPaper','reanalysis');

% initialize matrix to store data
data_h=zeros(n_vertices,n_subs_h);
data_m=zeros(n_vertices,n_subs_m);

fprintf('load data ...\n');
% load human
for sub=1:n_subs_h
    filename = fullfile(filesep, datadir,'human',['subj',num2str(sub)],[tract,'_',hemi,'_S4.func.gii']);
    data_h(:,sub)=readimgfile(filename);
end

% load macaque
for sub=1:n_subs_m
    filename = fullfile(filesep, datadir,'macaque',subs_m{sub},[tract,'_',hemi,'_S2.func.gii']);
    data_m(:,sub)=readimgfile(filename);
end

% compute ranksum test to compare species

fprintf('compute ranksum test ...\n');
p=zeros(n_vertices);

for i=1:n_vertices
    [p(i),h(i),stats(i)]=ranksum(data_h(i,:),data_m(i,:),'method','exact');
end
out=gifti(p');
save(out,fullfile(outdir,'pvals.func.gii'));

%% mask pvals
% wb_command -metric-mask pvals.func.gii af_avg_l_S4_logNorm_t0.9.func.gii pvals_masked.func.gii

filename=fullfile(outdir,'pvals_masked.func.gii');
data=readimgfile(filename);

thres=fdr(p);

percentage=(length(data(data<thres))-length(data(data==0)))/length(data(data~=0))*100;
